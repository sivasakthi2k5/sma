import express from 'express'
import bodyparser from 'body-parser'
import cors from 'cors'
import https from 'https'
import Stocks from './stocks.js'
import fs from 'fs'
import mongodb from 'mongodb'

const app = express()
app.use(cors())
app.use(bodyparser.urlencoded({extended:true}))
app.set('view engine', 'ejs')

const API_KEY = 'HNSRPHUTPQC0MHTG'
const alphaVantageInterface = new Stocks(API_KEY)

const stockList = ['ABB','ACC','ADANIENT','ADANIPORTS','ADANIPOWER','AJANTPHARM','ALBK','AMARAJABAT','AMBUJACEM','ANDHRABANK','APOLLOHOSP','APOLLOTYRE','ARVIND','ASHOKLEY','ASIANPAINT','AUROPHARMA','AXISBANK','BAJAJFINSV','BAJFINANCE','BALKRISIND','BALRAMCHIN','BANKBARODA','BANKINDIA','BATAINDIA','BEML','BERGEPAINT','BEL','BHARATFIN','BHARATFORG','BPCL','BHARTIARTL','INFRATEL','BHEL','BIOCON','BOSCHLTD','BRITANNIA','CADILAHC','CANFINHOME','CANBK','CAPF','CASTROLIND','CEATLTD','CENTURYTEX','CESC','CGPOWER','CHENNPETRO','CHOLAFIN','CIPLA','COALINDIA','COLPAL','CONCOR','CUMMINSIND','DABUR','DALMIABHA','DCBBANK','DMART','DHFL','DISHTV','DIVISLAB','DLF','DRREDDY','EICHERMOT','EMAMILTD','ENGINERSIN','EQUITAS','ESCORTS','EXIDEIND','FEDERALBNK','GAIL','GLAXO','GLENMARK','GMRINFRA','GODFRYPHLP','GODREJCP','GODREJIND','GRANULES','GRASIM','GSFC','GSKCONS','HAVELLS','HCLTECH','HDFCBANK','HDFC','HEROMOTOCO','HEXAWARE','HINDALCO','HCC','HINDPETRO','HINDUNILVR','HINDZINC','ICICIBANK','ICICIPRULI','IDBI','IDEA','IDFCBANK','IDFC','IFCI','IBULHSGFIN','INDIANB','IOC','IGL','INDUSINDBK','INFIBEAM','INFY','INDIGO','IRB','ITC','JISLJALEQS','JPASSOCIAT','JETAIRWAYS','JINDALSTEL','JSWSTEEL','JUBLFOOD','JUSTDIAL','KAJARIACER','KTKBANK','KSCL','KOTAKBANK','KPIT','LT','LICHSGFIN','LUPIN','MGL','MANAPPURAM','MRPL','MARICO','MARUTI','MFSL','MINDTREE','MOTHERSUMI','MRF','MCX','MUTHOOTFIN','NATIONALUM','NBCC','NCC','NESTLEIND','NHPC','NIITTECH','NMDC','NTPC','ONGC','OIL','OFSS','ORIENTBANK','PAGEIND','PCJEWELLER','PETRONET','PIDILITIND','PEL','PFC','POWERGRID','PTC','PNB','PVR','RAYMOND','RBLBANK','RELCAPITAL','RCOM','RELIANCE','RELINFRA','RPOWER','REPCOHOME','RECLTD','SHREECEM','SRTRANSFIN','SIEMENS','SREINFRA','SRF','SBIN','SAIL','STAR','SUNPHARMA','SUNTV','SUZLON','SYNDIBANK','TATACHEM','TATACOMM','TCS','TATAELXSI','TATAGLOBAL','TATAMTRDVR','TATAMOTORS','TATAPOWER','TATASTEEL','TECHM','INDIACEM','RAMCOCEM','SOUTHBANK','TITAN','TORNTPHARM','TORNTPOWER','TV18BRDCST','TVSMOTOR','UJJIVAN','ULTRACEMCO','UNIONBANK','UBL','UPL','VEDL','VGUARD','VOLTAS','WIPRO','WOCKPHARMA','YESBANK','ZEEL']
const novHoliday = [1326,1462.3,165.75,326.2,49.6,1063.4,43.8,779.05,205.6,30.15,1131.7,220.25,318.65,121.85,1250.95,794.2,606.85,5628.2,2348.4,938.1,105.45,110.95,86.35,1014.25,713.3,299.2,95.45,911.9,592.5,288.75,306.25,265.15,69.7,632.7,19482.25,5810.45,355,280.2,242.8,480,146.15,1170.25,867.1,684.45,36.4,270.8,1293.5,533.35,266.95,1103.45,667.5,778.65,369.3,2186.95,166.45,1425.15,230.5,42.75,1497.5,172.95,2452.95,22650.3,421.25,119.3,105.9,678.25,255.75,80.95,375.15,1383.2,665.55,16.6,784.3,705.4,473.55,103.9,858.25,114.35,7101.6,648.25,1031.55,1957.95,1815.85,2888.55,312.4,244.05,13.7,229.6,1643.85,262.95,355.9,359.85,59.55,42.1,35.5,37.75,14.2,831.9,261.6,138.95,274.75,1483.9,49.9,675.5,961.2,141.2,280.05,74.3,6.75,258.8,183.8,351.65,1047.8,491.9,422.15,105.85,502.25,1134.3,220.55,1365.95,429.9,852.05,844.85,86.65,83,339.65,7127.9,393.4,836.6,171.5,65564.95,706.1,447.4,70.65,64.1,81.4,10220.4,24.95,1170.4,113.15,154.85,157,198.6,3565.3,82.2,29535.05,83.4,218.55,1011.65,2201.45,98.25,189.05,78.8,70.95,1417.45,755.15,540.1,251.05,13.85,1110.7,359.4,30.9,404.55,119.1,15371.4,1223.3,941.55,36.05,2002.8,286.55,66.6,428.2,582.3,609.8,6.2,34.85,702.35,503.1,1942.15,1056.75,215.2,105.7,194.85,75.95,580.25,697.25,94.05,627.55,15.15,850.15,1667.15,265.25,37.8,552.4,207.35,3782.6,79.3,1255.3,729.6,209.75,196.45,513.9,325.35,510.05,215.95,446.85]
let stockListIndex = 0

const MONGODB_URL = 'mongodb://admin:admin000@ds139334.mlab.com:39334/stock-details'
const MongoClient = mongodb.MongoClient
let databaseConnection
MongoClient.connect(MONGODB_URL, (error, database) => {
  if (error) return console.log('error connecting DB')
  databaseConnection = database.db('stock-details')
})

app.get('/', (req, res, next) => {
  res.sendFile(__dirname + '/index.html')
})
app.post('/crossoverlist', (req, res, next) => {
  const movingAverage1 = req.body.first_moving_average
  const movingAverage2 = req.body.second_moving_average
  let prevSMA, currentSMA, stockListCode =[]
  databaseConnection.collection('closing-price-26-12').find().toArray((error, result)=> {
    if (error) return console.log(error)
    for(let index = 0;index < result.length - 1;index++) {
      const currentSMA1 = result[index].currentSMA[movingAverage1 -1]
      const prevSMA1 = result[index].prevSMA[movingAverage1 - 1]
      const currentSMA2 = result[index].currentSMA[movingAverage2 -1]
      const prevSMA2 = result[index].prevSMA[movingAverage2 -1]

      currentSMA1 - currentSMA2 < 0 ? prevSMA = false : prevSMA = true
      prevSMA1 - prevSMA2 < 0 ? currentSMA = false : currentSMA = true
      if(prevSMA ? !currentSMA : currentSMA) {
        console.log(result[index.stockCode])
        stockListCode.push(result[index].stockCode)
      }
    }
    res.render('crossover.ejs', {stockListCode:stockListCode})
  })
})
app.get('/dailysma', async(req, res, next) => {
  if(!stockListIndex) {
    console.log('inside dailysma')
    const closingPriceOperation = setInterval(async () => {
      let currentSMA = [], prevSMA = [], priceIndex, averageIndex, priceTotal, dbInput
      let smaQueryParam = {
        amount: 51,
        interval: 'daily',
        symbol: 'NSE:' + stockList[stockListIndex]
      }
      const result = await alphaVantageInterface.timeSeries(smaQueryParam)
      for(priceIndex = 0;priceIndex < smaQueryParam.amount - 1;priceIndex++) {
        priceTotal = result[0]
        for(averageIndex = priceIndex;averageIndex > 0;averageIndex--) {
          if(result[averageIndex] === 0)
            priceTotal += result[averageIndex] + novHoliday[stockListIndex]
          else
            priceTotal += result[averageIndex]
        }
        currentSMA.push(priceTotal/(priceIndex + 1))
      }
      for(priceIndex = 1;priceIndex < smaQueryParam.amount - 1;priceIndex++) {
        priceTotal = result[1]
        for(averageIndex = priceIndex;averageIndex > 1;averageIndex--) {
          if(result[averageIndex] === 0)
          priceTotal += result[averageIndex] + novHoliday[stockListIndex]
          else
            priceTotal += result[averageIndex]
        }
        prevSMA.push(priceTotal/(priceIndex))
      }
      dbInput = {
        index: stockListIndex,
        stockCode: stockList[stockListIndex],
        currentSMA,
        prevSMA,
      }
      databaseConnection.collection('closing-price-26-12').insertOne(dbInput, (error, result) => {
        if (error) return console.log('error connecting collection')
        console.log(stockList[stockListIndex] + ' - ' + stockListIndex)
      })
      stockListIndex++
      stockListIndex === 205 && clearInterval(closingPriceOperation)
    }, 12000)
  }
})
app.get('/hourlysma', async(req, res, next) => {
  if(!stockListIndex) {
    console.log('inside hourlysma')
    const hourlySMAInterval = setInterval(async () => {
      let currentHourlySMA = [], prevHourlySMA = [], priceIndex, averageIndex, priceTotal, dbInput
      let smaQueryParam = {
        amount: 51,
        interval: '60min',
        symbol: 'NSE:' + stockList[stockListIndex]
      }
      const result = await alphaVantageInterface.timeSeries(smaQueryParam)
      console.log(result)
      for(priceIndex = 0;priceIndex < smaQueryParam.amount - 1;priceIndex++) {
        priceTotal = result[0]
        for(averageIndex = priceIndex;averageIndex > 0;averageIndex--) {
          priceTotal += result[averageIndex]
        }
        currentHourlySMA.push(priceTotal/(priceIndex + 1))
      }
      for(priceIndex = 1;priceIndex < smaQueryParam.amount - 1;priceIndex++) {
        priceTotal = result[1]
        for(averageIndex = priceIndex;averageIndex > 1;averageIndex--) {
          priceTotal += result[averageIndex]
        }
        prevHourlySMA.push(priceTotal/(priceIndex))
      }
      dbInput = {
        index: stockListIndex,
        stockCode: stockList[stockListIndex],
        currentHourlySMA,
        prevHourlySMA,
      }
      databaseConnection.collection('hourlysma-26-12').insertOne(dbInput, (error, result) => {
        if (error) return console.log('error connecting collection')
        console.log(stockList[stockListIndex] + ' - ' + stockListIndex)
      })
      stockListIndex++
      stockListIndex === 205 && clearInterval(hourlySMAInterval)
    }, 12000)
  }
})
app.get('/sma', async(req, res, next) => {
  if(!stockListIndex) {
    console.log('inside sma')
    const smaOperation = setInterval(async () => {
      let smaQueryParam = {
        amount: 15,
        interval: '60min',
        indicator: 'SMA',
        time_period: 5,
        series_type: 'close',
        symbol: 'NSE:' + stockList[stockListIndex]
      }
      console.log(stockList[stockListIndex] + ' - ' + stockListIndex)
      const fastResult = await alphaVantageInterface.technicalIndicator(smaQueryParam)
      smaQueryParam.time_period = 49
      const slowResult = await alphaVantageInterface.technicalIndicator(smaQueryParam)
      for(let index = 0;index < smaQueryParam.amount - 2;index++) {
        let prevSMA, currentSMA
        fastResult[index] - slowResult[index] < 0 ? prevSMA = false : prevSMA = true
        fastResult[index + 1] - slowResult[index + 1] < 0 ? currentSMA = false : currentSMA = true
        if(prevSMA ? !currentSMA : currentSMA) {
          console.log(stockList[stockListIndex] + ' - ' + prevSMA)
          fs.appendFile("todaylist.txt", stockList[stockListIndex] + '\n', (err) => {
            if (err) throw err;
          })
        }
      }
      stockListIndex++
      stockListIndex === 205 && clearInterval(smaOperation)
    }, 30000)
  }
})

app.listen(3001, () => console.log('App listening on port 3001!'))